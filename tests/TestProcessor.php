<?php

use Pharchivist\Pharchivist\Converter\PhabricatorTask;
use Pharchivist\Pharchivist\Converter\Processor;

class TestProcessor extends \PHPUnit\Framework\TestCase {

	/**
	 * @dataProvider provideExtractFields
	 */
	public function testExtractFields( array $taskData, ?PhabricatorTask $expectedTask ) {
		$processor = new Processor(null);
		$task = $processor->makeTask( $taskData );
		if ( $expectedTask === null ) {
			$this->assertNull( $task );
		} else {
			$this->assertEquals($expectedTask, $task);
		}
	}

	/**
	 * @return array[]
	 */
	public function provideExtractFields() {
		return [
			[
				[
					'id' => 123,
					'type' => 'TASK',
					'fields' => [
						'name' => 'Foo',
						'priority' => [
							'name' => 'Unbreak Now!'
						],
						'description' => [
							'raw' => '#markdown'
						],
						'policy' => [
							'view' => 'public'
						]
					],

				],
				new PhabricatorTask( 123, 'Foo', 'Unbreak Now!', '#markdown' )
			],
			[
				[
					'id' => 1,
					'type' => 'TASK',
					'fields' => [
						'name' => 'Restricted',
						'priority' => [
							'name' => 'High',
						],
						'description' => [
							'raw' => 'secret',
						],
						'policy' => [
							'view' => 'secret'
						]
					]
				],
				null
			],
			[
				[
					'id' => 2,
					'type' => 'FOO',
				],
				null
			]
		];
	}

}
