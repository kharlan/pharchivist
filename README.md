# 🗃️ pharchivist

A console application for downloading Phabricator tasks and exporting to
various locations and formats (JSON, Markdown, and Meilisearch).

## Prerequisites

* Install [arcanist](https://secure.phabricator.com/book/phabricator/article/arcanist/#installing-arcanist)
and verify that it works and is in your $PATH.
* Set up a Redis instance. On macOS: `brew install redis` and `brew services start redis`
* Set up a Meilisearch instance. On macOS: `brew install meilisearch` and `brew services start meilisearch`

## Getting started

`composer install` to download dependencies.

Run `bin/pharchivist meilisearch:setup` to configure the Meilisearch index:

```
bin/pharchivist meilisearch:setup
{"taskUid":22703,"indexUid":"tasks","status":"enqueued","type":"indexCreation","enqueuedAt":"2022-09-11T20:09:57.817069Z"}
{"taskUid":22704,"indexUid":"tasks","status":"enqueued","type":"settingsUpdate","enqueuedAt":"2022-09-11T20:09:57.819815Z"}
{"taskUid":22705,"indexUid":"tasks","status":"enqueued","type":"settingsUpdate","enqueuedAt":"2022-09-11T20:09:57.822568Z"}
```

You can check the status of the tasks:

```
curl  -X GET http://localhost:7700/tasks/22705
{"uid":22705,"indexUid":"tasks","status":"succeeded","type":"settingsUpdate","details":{"displayedAttributes":["projects","id","author","description","owner","dateCreated","dateClosed","priority","name","subscribers"]},"duration":"PT0.000850S","enqueuedAt":"2022-09-11T20:09:57.822568Z","startedAt":"2022-09-11T20:09:57.823238Z","finishedAt":"2022-09-11T20:09:57.824088Z"}
```

## Downloading tasks

Run `bin/pharchivist download --limit=10`.

## Export tasks

* To JSON: `bin/pharchivist export:json`
* As Markdown: `bin/pharchivst export:markdown`
* Post to a meilisearch instance: `bin/pharcivist export:meilisearch`

## Troubleshooting

The application uses Redis as a cache for phabricator objects as well as a store for compiled data for a task.

To inspect what the application stores, use `redis-cli`:

* Content store: `keys pharchivist.task.*`
* Cache: `keys PHID-*`
