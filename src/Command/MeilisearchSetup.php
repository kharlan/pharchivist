<?php

namespace Pharchivist\Pharchivist\Command;

use MeiliSearch\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MeilisearchSetup extends Command
{
    public function getName()
    {
        return 'meilisearch:setup';
    }

    public function configure()
    {
        $this->addOption(
            'host',
            null,
            InputOption::VALUE_REQUIRED,
            'Meilisearch host',
            'http://127.0.0.1:7700'
        );
        $this->addOption(
            'api-key',
            null,
            InputOption::VALUE_REQUIRED,
            'The API key to use with meilisearch'
        );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $client = new Client($input->getOption('host'), $input->getOption('api-key')) ;
        $result = $client->createIndex('tasks', [ 'primaryKey' => 'id' ]);
        $output->writeln('<info>' . json_encode($result) . '</info>');
        // TODO: Could check for task completion. But let's be lazy about it for now
        // as index creation is fast.
        sleep(5);
        $result = $client->getIndex('tasks')->updateFilterableAttributes([
                'projects',
                'owner',
                'author',
                'status'
        ]);
        $output->writeln('<info>' . json_encode($result) . '</info>');
        $result = $client->getIndex('tasks')->updateDisplayedAttributes([
            'projects',
            'id',
            'author',
            'description',
            'owner',
            'dateCreated',
            'dateClosed',
            'priority',
            'name',
            'subscribers'
        ]);
        $output->writeln('<info>' . json_encode($result) . '</info>');
        return Command::SUCCESS;
    }
}
