<?php

namespace Pharchivist\Pharchivist\Command;

use MeiliSearch\Client;
use MeiliSearch\Endpoints\Indexes;
use MeiliSearch\Exceptions\ApiException;
use Pharchivist\Pharchivist\Converter\PhabricatorTask;
use Pharchivist\Pharchivist\Converter\TaskStore;
use Redis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportMeilisearch extends Command
{
    private const DEFAULT_MAX_LIMIT = 1000000;

    public function getName()
    {
        return 'export:meilisearch';
    }

    public function configure()
    {
        $this->addOption(
            'limit',
            'l',
            InputOption::VALUE_REQUIRED,
            'The limit of tasks to get.',
            self::DEFAULT_MAX_LIMIT
        );
        $this->addOption(
            'task-ids',
            null,
            InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
            'If set, export only these task IDs',
            []
        );
        $this->addOption(
            'api-key',
            null,
            InputOption::VALUE_REQUIRED,
            'The API key to use with meilisearch'
        );
        $this->addOption(
            'meilisearch-host',
            null,
            InputOption::VALUE_REQUIRED,
            'Meilisearch host',
            'http://127.0.0.1:7700'
        );
        $this->addOption(
            'overwrite',
            null,
            InputOption::VALUE_OPTIONAL,
            'Whether to overwrite existing entries',
            false
        );
        $this->addOption(
            'data-directory',
            null,
            InputOption::VALUE_REQUIRED,
            'The directory used by the task store.',
            dirname(__DIR__, 2) . '/data/json'
        );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $client = new Client($input->getOption('meilisearch-host'), $input->getOption('api-key')) ;
        $client->createIndex('tasks');
        $index = $client->index('tasks');
        $count = 0;
        $limit = $input->getOption('limit');
        $iterator = null;
        $taskIds =  $input->getOption('task-ids');
        $taskStore = new TaskStore($input->getOption('data-directory'));

        if ($taskIds) {
            foreach ($taskIds as $taskId) {
                $phabTask = $taskStore->get($taskId);
                if (!$phabTask instanceof PhabricatorTask) {
                    $output->writeln('<error>Did not find task ID ' . $taskId . '</error>');
                    continue;
                }
                $this->addDocument($index, $phabTask, $output);
            }
            return Command::SUCCESS;
        }

        foreach ($taskStore->scan() as $filename) {
            if (strpos($filename, '.json') === false) {
                continue;
            }
            if ($count >= $limit) {
                $output->writeln(PHP_EOL . '<info>Reached limit!</info>');
                break;
            }
            $taskId = str_replace('.json', '', $filename);
            $phabTask = $taskStore->get($taskId);
            if (!$phabTask instanceof PhabricatorTask) {
                $output->writeln(PHP_EOL . '<error>Could not find data for ' . $taskId . '</error>');
                continue;
            }
            try {
                $index->getDocument($phabTask->getId());
                if (!$input->getOption('overwrite')) {
                    $output->writeln('<info>Skipping task ' . $phabTask->getId() . '</info>');
                    continue;
                }
            } catch (ApiException $apiException) {
                // Item isn't found; that's fine, we'll add it now.
            }
            $this->addDocument($index, $phabTask, $output);
        }
        return Command::SUCCESS;
    }

    /**
     * @param Indexes $index
     * @param PhabricatorTask $phabTask
     * @param OutputInterface $output
     * @return void
     */
    private function addDocument(Indexes $index, PhabricatorTask $phabTask, OutputInterface $output): void
    {
        $result = $index->addDocumentsJson(json_encode($phabTask));
        $output->writeln('<info>' . json_encode($result) . '</info>');
        $output->writeln('<info>Added task ' . $phabTask->getId() . ' to index.</info>');
    }
}
