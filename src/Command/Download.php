<?php

namespace Pharchivist\Pharchivist\Command;

use Pharchivist\Pharchivist\Converter\Api;
use Pharchivist\Pharchivist\Converter\Cache;
use Pharchivist\Pharchivist\Converter\PhabricatorTask;
use Pharchivist\Pharchivist\Converter\Processor;
use Pharchivist\Pharchivist\Converter\TaskStore;
use Redis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class Download extends Command
{
    private const DEFAULT_MAX_LIMIT = 1000000;

    public function getName(): string
    {
        return 'download';
    }

    public function getDescription()
    {
        return 'Download Phabricator tasks and stash in a Redis cache for further processing.';
    }

    public function configure()
    {
        $this->addOption(
            'limit',
            'l',
            InputOption::VALUE_REQUIRED,
            'The limit of tasks to get.',
            self::DEFAULT_MAX_LIMIT
        );
        $this->addOption(
            'after',
            null,
            InputOption::VALUE_REQUIRED,
            'The value to use in "after" for the query',
            null
        );
        $this->addOption(
            'before',
            null,
            InputOption::VALUE_REQUIRED,
            'The value to use in "before" for the query',
            null
        );
        $this->addOption(
            'overwrite',
            null,
            InputOption::VALUE_OPTIONAL,
            'Whether to overwrite data.',
            true
        );
        $this->addOption(
            'order',
            null,
            InputOption::VALUE_REQUIRED,
            'The ordering to use with the query.',
            null
        );
        $this->addOption(
            'redis-host',
            null,
            InputOption::VALUE_REQUIRED,
            'Redis host containing cache data',
            '127.0.0.1'
        );
        $this->addOption(
            'data-directory',
            null,
            InputOption::VALUE_REQUIRED,
            'The directory used by the task store.',
            dirname(__DIR__, 2) . '/data/json'
        );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $taskStore = new TaskStore($input->getOption('data-directory'));
        $api = new Api(new Cache(new Redis(), $input->getOption('redis-host')));

        $query = [ 'queryKey' => 'all'];
        // Batching limit, not the total limit.
        $query['limit'] = 10;
        if ($input->getOption('after')) {
            $query['after'] = $input->getOption('after');
        }
        if ($input->getOption('before')) {
            $query['before'] = $input->getOption('before');
        }
        if ($input->getOption('order')) {
            $query['order'] = $input->getOption('order');
        }
        $limit = $input->getOption('limit');
        $count = 0;

        $searchData = $api->getManiphestSearch($query);
        if (!isset($searchData['data'])) {
            // Sometimes arc fails. Wait a few seconds, and restart.
            $output->writeln('<info>Restarting...</info>');
            $this->continue($count, $limit, $query['after'], $input, $output);
        }
        $tasks = $searchData['data'];

        $processor = new Processor($api);

        $progress = new ProgressBar($output, count($tasks));

        $processedTasks = [];
        $skipped = [];

        foreach ($tasks as $task) {
            if ($count >= $limit) {
                $output->writeln(PHP_EOL . '<info>Reached limit!</info>');
                break;
            }

            $taskId = 'T' . $task['id'];
            if (!$input->getOption('overwrite') && $taskStore->get($taskId) instanceof PhabricatorTask) {
                $skipped[] = $task['id'];
                continue;
            }
            $phabTask = $processor->makeTask($task);
            if (!$phabTask) {
                $output->writeln('<comment>Skipped T' . $task['id'] . '</comment>');
                continue;
            }

            $progress->display();
            $count++;
            $taskStore->set($phabTask);
            $progress->advance();
            $processedTasks[$phabTask->getId()] = $phabTask->getName();
        }

        if ($processedTasks) {
            $progress->finish();
        }
        $apiCalls = $api->getApiCallCount();
        $output->writeln(PHP_EOL . "<info>$count tasks processed with $apiCalls API calls.</info>");
        if (count($skipped)) {
            $output->writeln('<comment>Skipped tasks ' . implode(', ', $skipped) . ' as they already exist in Redis storage.</comment>');
        }
        if (count($processedTasks)) {
            $table = new Table($output);
            $table->setHeaders(['ID', 'Title']);
            foreach ($processedTasks as $taskId => $processedTask) {
                $table->addRow([$taskId, $processedTask]);
            }
            $table->render();
        }
        $this->continue($count, $limit, $searchData['cursor']['after'], $input, $output);
        return Command::SUCCESS;
    }

    /**
     * @param int $count
     * @param $limit
     * @param $after
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    private function continue(int $count, $limit, $after, InputInterface $input, OutputInterface $output): void
    {
        if ($count < $limit && $after) {
            $input->setOption('after', $after);
            $input->setOption('limit', $input->getOption('limit') - $count);
            $output->writeln(PHP_EOL . '<info>Continuing processing after cursor position ' . $after . '</info>');
            $this->execute($input, $output);
        }
    }
}
