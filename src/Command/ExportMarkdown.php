<?php

namespace Pharchivist\Pharchivist\Command;

use Pharchivist\Pharchivist\Converter\Api;
use Pharchivist\Pharchivist\Converter\Cache;
use Pharchivist\Pharchivist\Converter\PhabricatorTask;
use Pharchivist\Pharchivist\Converter\Processor;
use Pharchivist\Pharchivist\Converter\TaskStore;
use Redis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportMarkdown extends \Symfony\Component\Console\Command\Command
{
    private const DEFAULT_MAX_LIMIT = 1000000;

    public function getName()
    {
        return 'export:markdown';
    }

    public function configure()
    {
        $this->addOption(
            'data-directory',
            null,
            InputOption::VALUE_REQUIRED,
            'The directory used by the task store.',
            dirname(__DIR__, 2) . '/data'
        );
        $this->addOption(
            'limit',
            'l',
            InputOption::VALUE_REQUIRED,
            'The limit of tasks to get.',
            self::DEFAULT_MAX_LIMIT
        );
        $this->addOption(
            'overwrite',
            null,
            InputOption::VALUE_OPTIONAL,
            'Whether to overwrite existing files.',
            false
        );
        $this->addOption(
            'redis-host',
            null,
            InputOption::VALUE_REQUIRED,
            'Redis host containing cache data',
            '127.0.0.1'
        );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $redis = new Redis();
        $api = new Api(new Cache($redis, $input->getOption('redis-host')));
        $processor = new Processor($api);
        $redis->connect($input->getOption('redis-host'));
        $dataDir = $input->getOption('data-directory');
        $markdownDir = $dataDir . '/markdown';
        $iterator = null;
        $limit = $input->getOption('limit');
        $count = 0;
        $taskStore = new TaskStore($dataDir . '/json');
        $processedTasks = [];
        foreach ($taskStore->scan() as $filename) {
            if (strpos($filename, '.json') === false) {
                continue;
            }
            if ($count >= $limit) {
                $output->writeln(PHP_EOL . '<info>Reached limit!</info>');
                break;
            }
            $taskId = str_replace('.json', '', $filename);
            $phabTask = $taskStore->get($taskId);
            if (!$phabTask instanceof PhabricatorTask) {
                continue;
            }
            $markdownFilename = $markdownDir . '/' . $phabTask->getId() . '.md';
            if (!$input->getOption('overwrite') && file_exists($markdownFilename)) {
                $output->writeln('<info>Skipping task ' . $phabTask->getId() . '</info>');
                continue;
            }
            $content = $processor->getMarkdown($phabTask);
            file_put_contents($markdownFilename, $content, LOCK_EX);
            $processedTasks[$phabTask->getId()] = $phabTask->getName();
            $count++;
        }
        if (count($processedTasks)) {
            $table = new Table($output);
            $table->setHeaders(['ID', 'Title']);
            foreach ($processedTasks as $taskId => $processedTask) {
                $table->addRow([$taskId, $processedTask]);
            }
            $table->render();
        }
        return Command::SUCCESS;
    }
}
