<?php

namespace Pharchivist\Pharchivist\Converter;

use DirectoryIterator;

class TaskStore
{
    private string $dataDirectory;

    /**
     * @param string $dataDirectory
     */
    public function __construct(string $dataDirectory)
    {
        $this->dataDirectory = $dataDirectory;
    }

    public function set(PhabricatorTask $phabricatorTask)
    {
        $file = $this->dataDirectory . '/' . $phabricatorTask->getId() . '.json';
        file_put_contents($file, json_encode($phabricatorTask));
    }

    public function get($taskId): ?PhabricatorTask
    {
        $file = $this->dataDirectory . '/' . "$taskId.json";
        if (!file_exists($file)) {
            return null;
        }
        $data = file_get_contents($file);
        $decoded = json_decode($data, true);
        if (!$decoded) {
            return null;
        }
        return PhabricatorTask::fromArray($decoded);
    }

    public function scan(): DirectoryIterator
    {
        return new DirectoryIterator($this->dataDirectory);
    }
}
