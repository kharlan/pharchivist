<?php

namespace Pharchivist\Pharchivist\Converter;

use JsonSerializable;

class PhabricatorTask implements JsonSerializable
{
    private $id;
    private string $name;
    private string $priority;
    private string $description;
    private int $dateCreated;
    private ?int $dateClosed;
    private string $status;
    private string $authorName;
    private ?string $ownerName;
    private array $projects;
    private array $subscribers;
    private array $comments;
    private ?string $descriptionHtml;

    /**
     * @param string|int $id
     * @param string $name
     * @param string $priority
     * @param string $description
     * @param string $status
     * @param int $dateCreated
     * @param int|null $dateClosed
     * @param string $authorName
     * @param string|null $ownerName
     * @param array $projects
     * @param array $subscribers
     * @param array $comments
     * @param string|null $descriptionHtml
     */
    public function __construct(
        $id,
        string $name,
        string $priority,
        string $description,
        string $status,
        int $dateCreated,
        ?int $dateClosed,
        string $authorName,
        ?string $ownerName,
        array $projects = [],
        array $subscribers = [],
        array $comments = [],
        ?string $descriptionHtml = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->priority = $priority;
        $this->description = $description;
        $this->descriptionHtml = $descriptionHtml;
        $this->dateCreated = $dateCreated;
        $this->dateClosed = $dateClosed;
        $this->status = $status;
        $this->authorName = $authorName;
        $this->ownerName = $ownerName;
        $this->projects = $projects;
        $this->subscribers = $subscribers;
        $this->comments = $comments;
    }

    public function jsonSerialize()
    {
        $id = (string)$this->getId();
        if (strpos($id, 'T') !== 0) {
            $id = 'T' . $id;
        }
        return [
            'id' => $id,
            'name' => $this->getName(),
            'priority' => $this->getPriority(),
            'description' => $this->getDescription(),
            'descriptionHtml' => $this->getDescriptionHtml(),
            'status' => $this->getStatus(),
            'dateCreated' => $this->getDateCreated(),
            'dateClosed' => $this->getDateClosed(),
            'author' => $this->getAuthorName(),
            'owner' => $this->getOwnerName(),
            'projects' => $this->getProjects(),
            'subscribers' => $this->getSubscribers(),
            'comments' => $this->getComments(),
        ];
    }

    public static function fromArray(array $phabTaskData): PhabricatorTask
    {
        $id = (string)$phabTaskData['id'];
        if (strpos($id, 'T') !== 0) {
            $id = 'T' . $id;
        }
        return new PhabricatorTask(
            $id,
            $phabTaskData['name'],
            $phabTaskData['priority'],
            $phabTaskData['description'],
            $phabTaskData['status'],
            $phabTaskData['dateCreated'],
            $phabTaskData['dateClosed'] ?? null,
            $phabTaskData['author'],
            $phabTaskData['ownerName'] ?? null,
            $phabTaskData['projects'],
            $phabTaskData['subscribers'],
            $phabTaskData['comments'] ?? [],
            $phabTaskData['descriptionHtml'] ?? null
        );
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPriority(): string
    {
        return $this->priority;
    }


    /**
     * @return int
     */
    public function getDateCreated(): int
    {
        return $this->dateCreated;
    }

    /**
     * @return int|null
     */
    public function getDateClosed(): ?int
    {
        return $this->dateClosed;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAuthorName(): string
    {
        return $this->authorName;
    }

    /**
     * @return string|null
     */
    public function getOwnerName(): ?string
    {
        return $this->ownerName;
    }

    /**
     * @return array
     */
    public function getProjects(): array
    {
        return $this->projects;
    }

    /**
     * @return array
     */
    public function getSubscribers(): array
    {
        return $this->subscribers;
    }

    /**
     * @return array
     */
    public function getComments(): array
    {
        return $this->comments;
    }

    /**
     * @param array $comments
     */
    public function setComments(array $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return string|null
     */
    public function getDescriptionHtml(): ?string
    {
        return $this->descriptionHtml;
    }
}
