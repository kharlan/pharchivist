<?php

namespace Pharchivist\Pharchivist\Converter;

class Processor
{
    private Api $api;

    private const UNKNOWN_USER = 'user';

    /**
     * @param Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @param array $data Raw data from Phabricator API
     * @return PhabricatorTask|null
     */
    public function makeTask(array $data): ?PhabricatorTask
    {
        if ($data['type'] !== 'TASK') {
            return null;
        }
        if ($data['fields']['policy']['view'] !== 'public') {
            return null;
        }
        $authorPHID = $data['fields']['authorPHID'];
        $authorData = $this->api->getPhidQuery([ $authorPHID ]);
        $authorName = $authorData[$authorPHID]['name'] ?? self::UNKNOWN_USER;
        $ownerName = null;
        $ownerPHID =  $data['fields']['ownerPHID'] ?? null;
        if ($ownerPHID) {
            $ownerData = $this->api->getPhidQuery(
                [ $data['fields']['ownerPHID'] ]
            );
            $ownerName = $ownerData[$ownerPHID]['data']['fields']['ownerPHID']['name'] ?? null;
        }
        $projectInfo = $this->api->getManiphestInfo($data['id']);
        $subscribers = [];
        foreach ($projectInfo['ccPHIDs'] ?? [] as $ccPHID) {
            $userInfo = $this->api->getPhidQuery([ $ccPHID ]);
            $subscribers[] = $userInfo[$ccPHID]['name'] ?? null;
        }
        array_filter($subscribers);

        $projectData = $this->lookupProjects($data['id']);
        $projectNames = [];
        if ($projectData) {
            foreach ($projectData as $project) {
                $projectNames[] = $project['name'];
            }
        }

        $descriptionHtml = $this->api->getRemarkupProcess([$data['fields']['description']['raw']]);

        $comments = [];
        $task = new PhabricatorTask(
            'T' . $data['id'],
            $data['fields']['name'],
            $data['fields']['priority']['name'],
            $data['fields']['description']['raw'],
            $data['fields']['status']['name'],
            $data['fields']['dateCreated'],
            $data['fields']['dateClosed'],
            $authorName,
            $ownerName,
            $projectNames,
            $subscribers,
            [],
            $descriptionHtml
        );
        $this->getTaskComments($task, $comments);
        foreach ($comments as $comment) {
            $commentHtml = $comment;
            $commentHtml['html'] = $this->api->getRemarkupProcess([$comment['content']]);
        }
        $task->setComments($comments);
        return $task;
    }


    /**
     * @param int $taskId
     * @return array|null
     */
    private function lookupProjects(int $taskId): ?array
    {
        $maniphestInfo = $this->api->getManiphestInfo($taskId);
        $projectPhids = $maniphestInfo['projectPHIDs'] ?? null;
        if ($projectPhids) {
            return $this->api->getPhidQuery($projectPhids);
        }
        return null;
    }

    /**
     * @param PhabricatorTask $task
     * @return string
     */
    public function getMarkdown(PhabricatorTask $task): string
    {
        $metadata = '---' . PHP_EOL;
        $metadata .= 'title: ' . $task->getName() . PHP_EOL;
        $metadata .= 'link: ' . 'https://phabricator.wikimedia.org/' . $task->getId() . PHP_EOL;
        $metadata .= 'status: ' . $task->getStatus() . PHP_EOL;
        $metadata .= 'created: ' . date('c', $task->getDateCreated()) . PHP_EOL;
        if ($task->getDateClosed()) {
            $metadata .= 'closed: ' . date('c', $task->getDateClosed()) . PHP_EOL;
        }
        if ($task->getOwnerName()) {
            $metadata .= 'owner: ' . '@' . $task->getOwnerName() . PHP_EOL;
        }
        $metadata .= 'author: ' . '@' . $task->getAuthorName() . PHP_EOL;
        $metadata .= 'priority: ' . $task->getPriority() . PHP_EOL;
        foreach ($task->getProjects() as $project) {
            $metadata .= 'project: ' . $project . PHP_EOL;
        }
        foreach ($task->getSubscribers() as $subscriber) {
            $metadata .= 'subscriber: ' . $subscriber . PHP_EOL;
        }
        $metadata .= '---' . PHP_EOL;
        $content = '# ' . $task->getName() . PHP_EOL . PHP_EOL;
        $content .= $task->getDescription();
        $taskComments = $task->getComments();
        foreach ($taskComments as $comment) {
            $content .= '## ' . '@' . $comment['author'] . ' on ' . date('c', $comment['dateCreated']) . PHP_EOL . PHP_EOL;
            $content .= $comment['content'] . PHP_EOL . PHP_EOL;
        }
        return $metadata . PHP_EOL . $content;
    }

    private function getTaskComments(PhabricatorTask $task, &$comments = [], $after = null): array
    {
        $transactions = $this->api->getTransactionSearch($task->getId());
        $comments = [];
        foreach ($transactions['data'] as $transaction) {
            if ($transaction['type'] !== 'comment') {
                continue;
            }
            $authorPHID = $transaction['authorPHID'];
            $phidQueryData = $this->api->getPhidQuery([$authorPHID]);
            $comments[] = [
                'author' =>	$phidQueryData[$authorPHID]['name'] ?? self::UNKNOWN_USER,
                'dateCreated' => $transaction['dateCreated'],
                'content' => $transaction['comments'][0]['content']['raw'],
                'html' => $this->api->getRemarkupProcess([$transaction['comments'][0]['content']['raw']])
            ];
        }
        if ($transactions['cursor']['after'] && $transactions['cursor']['after'] !== $after) {
            $comments += $this->getTaskComments($task, $comments, $transactions['cursor']['after']);
        }
        return array_reverse($comments);
    }
}
