<?php

namespace Pharchivist\Pharchivist\Converter;

use Symfony\Component\Process\Process;

class Api
{
    private Cache $cache;

    private int $apiCallCount = 0;

    /**
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function getManiphestInfo(int $taskId)
    {
        // No caching lookup for this one.
        $process = new Process(
            [
            'arc',
            'call-conduit',
            '--',
            'maniphest.info'
        ],
            null,
            null,
            json_encode([ 'task_id' => $taskId ])
        );
        $this->apiCallCount++;
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $out = $process->getOutput();
        $data = json_decode($out, true)['response'];
        if (!$data) {
            return [];
        }
        // cache the PHID data for later.
        $this->cache->set($data['phid'], $data);
        return $data;
    }

    public function getPhidQuery(array $phids)
    {
        $cachedData = $this->cache->getMulti($phids);
        $phidsToLookup = array_diff($phids, array_keys($cachedData));
        if (!$phidsToLookup) {
            return $cachedData;
        }
        $process = new Process(
            [
            'arc',
            'call-conduit',
            '--',
            'phid.query'
        ],
            null,
            null,
            json_encode([ 'phids' => $phidsToLookup ])
        );
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $this->apiCallCount++;
        $out = $process->getOutput();
        $data = json_decode($out, true)['response'];
        foreach ($phidsToLookup as $phid) {
            if (isset($data[$phid])) {
                $this->cache->set($phid, $data[$phid]);
            }
        }
        return array_merge($data, $cachedData);
    }

    /**
     * @return int
     */
    public function getApiCallCount(): int
    {
        return $this->apiCallCount;
    }

    public function getManiphestSearch(array $query): array
    {
        $process = new Process(
            [
                'arc',
                'call-conduit',
                '--',
                'maniphest.search'
            ],
            null,
            null,
            json_encode($query)
        );
        $this->apiCallCount++;
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $out = $process->getOutput();
        if (!$out || !json_decode($out)) {
            $error = $process->getErrorOutput();
            error_log($error);
            return [];
        }
        return json_decode($out, true)['response'];
    }

    public function getTransactionSearch(string $phid, $after = null)
    {
        $query = ['objectIdentifier' => $phid];
        if ($after) {
            $query['after'] = $after;
        }
        $process = new Process(
            [
                'arc',
                'call-conduit',
                '--',
                'transaction.search'
            ],
            null,
            null,
            json_encode($query)
        );
        $this->apiCallCount++;
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $out = $process->getOutput();
        return json_decode($out, true)['response'];
    }

    public function getRemarkupProcess(array $contents)
    {
        $query = [
            'context' => 'maniphest',
            'contents' => $contents
        ];
        $process = new Process(
            [
                'arc',
                'call-conduit',
                '--',
                'remarkup.process'
            ],
            null,
            null,
            json_encode($query)
        );
        $this->apiCallCount++;
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $out = $process->getOutput();
        return json_decode($out, true)['response'][0]['content'];
    }
}
