<?php

namespace Pharchivist\Pharchivist\Converter;

use Redis;

class Cache
{
    private Redis $redis;

    public function __construct(Redis $redis, string $host)
    {
        $this->redis = $redis;
        $this->redis->connect($host);
    }

    public function getMulti(array $phids)
    {
        $result = [];
        foreach ($phids as $phid) {
            $json = $this->redis->get($phid);
            $item = json_decode($json, true);
            if (!is_array($item)) {
                continue;
            }
            $result[$phid] = $item;
        }
        $result = array_filter($result);
        return $result;
    }

    public function set($key, array $data)
    {
        $this->redis->set($key, json_encode($data));
    }
}
